import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }  from "@angular/common/http"

import { AppComponent } from './app.component';
import { StartPage } from './pages/start/start.page';
import { DashboardPage } from './pages/dashboard/dashboard.page';

@NgModule({
  declarations: [
    AppComponent,
    StartPage,
    DashboardPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
