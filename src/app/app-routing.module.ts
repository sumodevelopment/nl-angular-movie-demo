import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { DashboardPage } from './pages/dashboard/dashboard.page';
import { StartPage } from './pages/start/start.page';

const routes: Routes = [
  {
    path: '',
    component: StartPage,
  },
  {
    path: 'dashboard',
    component: DashboardPage,
    canActivate: [ AuthGuard ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
