import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie } from '../models/movie.model';
import { User } from '../models/user.model';

const { apiKey, apiURL } = environment;

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) {
  }

  add(userId: number, movies: Movie[]): Observable<User> {
  
    const headers = new HttpHeaders({
      "content-type": "application/json",
      "x-api-key": apiKey,
    });

    return this.http.patch<User>(apiURL + "/" + userId, {
      favourites: [...movies]
    }, { headers })
  }


}
