import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiKey, apiURL } = environment;

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private http: HttpClient) {}

  public register(username: string): Observable<User> {

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey,
    });

    const user = {
      username,
      favourites: [],
    };

    return this.http.post<User>(apiURL, user, {
      headers
    }).pipe(
      map((user: User) => {
        if (!user) { // Throw an error if the user is not there
          throw 'User could not be created.';
        }
        return user;
      }),
      catchError((error) => { // Catch all errors
        // For HTTP errors (4xx, 5xx Response Codes)
        if (error instanceof HttpErrorResponse) {
          throw error.message;
        }
        // String Errors thrown in the map() 
        throw error;
        // Using throw will force it to enter the error:() => handler in the component. 
      })
    );
  }
}
