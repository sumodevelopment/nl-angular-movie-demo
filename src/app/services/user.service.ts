import { Injectable } from '@angular/core';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?: User;

  // getter/setter

  get user(): User | undefined {
    return this._user;
  }

  set user(user: User | undefined) {
    
    if (!user) {
      throw new Error("The user is undefined.");
    }

    sessionStorage.setItem("user", JSON.stringify(user));
    this._user = user;
  }

  constructor() { 
    const storedUser = sessionStorage.getItem("user");
    if (storedUser) {
      this._user = JSON.parse(storedUser);
    }
  }
}
