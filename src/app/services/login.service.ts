import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';

const { apiURL } = environment;

@Injectable({
  providedIn: 'root', // Singleton Instance
})
export class LoginService {
  constructor(private http: HttpClient) {}

  public login(username: string): Observable<User> {
    // HttpClient = Observable
    return this.http.get<User[]>(apiURL + '?username=' + username).pipe(
      map((response: User[]) => {
        const user = response.pop();
        if (!user) { // Throw an error if the user is not there
          throw 'User does not exist';
        }
        return user;
      }),
      catchError((error) => { // Catch all errors
        // For HTTP errors (4xx, 5xx Response Codes)
        if (error instanceof HttpErrorResponse) {
          throw error.message;
        }
        // String Errors thrown in the map() 
        throw error;

        // Using throw will force it to enter the error:() => handler in the component. 
      })
    );
  }
}
