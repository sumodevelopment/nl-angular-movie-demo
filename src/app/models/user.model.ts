import { Movie } from "./movie.model";

export interface User {
    id: number;
    username: string;
    favourites: Movie[];
  }