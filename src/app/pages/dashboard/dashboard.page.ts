import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { nanoid } from 'nanoid';
import { Movie } from 'src/app/models/movie.model';
import { User } from 'src/app/models/user.model';
import { MovieService } from 'src/app/services/movie.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {

  public loading: boolean = false;

  get user(): User | undefined {
    return this.userService.user;
  }

  constructor(private userService: UserService, private movieService: MovieService) { }

  ngOnInit(): void {
  }

  public onCreateSubmit(createForm: NgForm) {

    if (!this.userService.user) {
      return;
    }

    const { title, image } = createForm.value;
    
    const movie: Movie = {
      id: nanoid(),
      title,
      image
    };

    this.loading = true;
    this.movieService.add(
      this.userService.user?.id,
      [...this.userService.user.favourites, movie],
    ).subscribe({
      next: (user: User) => {
        this.userService.user = user;
      },
      complete: () => {
        this.loading = false;
      }
    })
    
  }

}
