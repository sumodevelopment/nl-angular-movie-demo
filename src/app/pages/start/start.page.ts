import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage {
  public loading: boolean = false;
  public error: string = '';

  // DI
  constructor(
    private loginService: LoginService,
    private registerService: RegisterService,
    private userService: UserService,
    private router: Router
  ) {}


  // Reusable Success handler for Register and Login.
  private handleSuccess(user: User) {
    this.userService.user = user;
    this.router.navigateByUrl('dashboard');
  }

  // Login with an existing user.
  public onStartSubmit(form: NgForm) {
    this.loading = true;

    const { username } = form.value;

    this.loginService.login(username).subscribe({
      next: (user: User) => this.handleSuccess(user),
      error: (error: string) => {
        this.error = error;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }

  // register a new user.
  public onRegisterClick(form: NgForm) {
    const { username } = form.value;
    this.loading = true;
    this.registerService.register(username).subscribe({
      next: (user: User) => this.handleSuccess(user),
      error: (error: HttpErrorResponse) => {
        this.error = error.message;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
}
